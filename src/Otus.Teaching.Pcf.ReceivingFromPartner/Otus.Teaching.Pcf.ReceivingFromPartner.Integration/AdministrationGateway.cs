﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.RabbitMqTransport.Integration;
using Otus.Teaching.Pcf.MessageBroker;
using Otus.Teaching.Pcf.MessageBroker.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly IMessageBrokerService _messageBrokerService;

        public AdministrationGateway(IMessageBrokerService messageBrokerService)
        {
            _messageBrokerService = messageBrokerService;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            await _messageBrokerService.PublishAsync(new NotifyAdminAboutPartnerManagerPromoCode {PartnerManagerId = partnerManagerId});
        }
    }
}