﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.MessageBroker.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class NotifyAdminAboutPartnerManagerPromoCodeConsumer : IConsumer<NotifyAdminAboutPartnerManagerPromoCode>
    {
        private readonly IRepository<Employee> _employeeRepository;

        public NotifyAdminAboutPartnerManagerPromoCodeConsumer(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task Consume(ConsumeContext<NotifyAdminAboutPartnerManagerPromoCode> context)
        {
            var employee = await _employeeRepository.GetByIdAsync(context.Message.PartnerManagerId);

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}