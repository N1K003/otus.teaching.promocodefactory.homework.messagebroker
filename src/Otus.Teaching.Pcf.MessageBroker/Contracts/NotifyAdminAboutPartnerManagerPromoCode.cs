﻿using System;

namespace Otus.Teaching.Pcf.MessageBroker.Contracts
{
    public class NotifyAdminAboutPartnerManagerPromoCode
    {
        public Guid PartnerManagerId { get; set; }
    }
}