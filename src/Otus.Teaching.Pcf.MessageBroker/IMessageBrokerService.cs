﻿using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.MessageBroker
{
    public interface IMessageBrokerService
    {
        Task PublishAsync<TMessage>(TMessage message, CancellationToken cancellationToken = default) where TMessage : class;
    }
}